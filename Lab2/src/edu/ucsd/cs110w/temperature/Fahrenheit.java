package edu.ucsd.cs110w.temperature;

public class Fahrenheit extends Temperature {
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		String s = (Float.toString(getValue()) + " F");
		return s;
	}
	@Override
	public Temperature toCelsius() {
		float v = ((getValue() - 32f) * .555556f);
		Temperature toC = new Celsius(v);
		return toC;
	}
	@Override
	public Temperature toFahrenheit() {
		return this;
	}
	@Override
	public Temperature toKelvin() {
		Temperature toK = new Kelvin(0.55556f*(getValue()- 32f)+273.15f);
		return toK;
	}
	
}
