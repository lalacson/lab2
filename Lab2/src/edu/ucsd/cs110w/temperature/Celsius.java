package edu.ucsd.cs110w.temperature;

public class Celsius extends Temperature {
	public Celsius(float t)
	{
		super(t);
	} public String toString()
	{
		String s = Float.toString(getValue()) + " C";
		return s;
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float val = (getValue() * 1.8f + 32.0f);
		Temperature toF = new Fahrenheit(val);
		return toF;
	}
	@Override
	public Temperature toKelvin() {
		Temperature toK = new Kelvin(getValue() + 273.15f);
		return toK;
	}
}
